import React, { Component } from 'react'
import {connect} from 'react-redux'
import {getProductos} from '../store/actions/productosAction'

class productos extends Component {
    componentDidMount(){
        this.props.getProductos()
    }
    render() {
        const {productos} = this.props.productos
        return (
            <div>
                {productos.map(u =>
                    <React.Fragment key={u.id}>
                        <h6 >{u.name}</h6>
                        <h6 >{u.descripcion}</h6>
                        <h6 >{u.price}</h6>
                    </React.Fragment>
                )}
            </div>
        )
    }
}

const mapStateToProps  = (state) => ({productos:state.productos})

export default connect(mapStateToProps, {getProductos})(productos)