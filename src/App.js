import React from 'react';

import './App.css';

import Productos from './component/listProducto'


function App() {
  return (
    <div className="App">
        <h1>Lista de Productos</h1>
        <Productos/>
    </div>
  );
}

export default App;