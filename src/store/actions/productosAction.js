import {GET_PRODUCTOS, PRODUCTOS_ERROR, POST_PRODUCTOS, DELETE_PRODUCTOS, UPDATE_PRODUCTOS} from '../types'
import axios from 'axios'
const url = 'localhost:9000/'

export const getProductos = () => async dispatch => {
    try{
        const res = await axios.get(url + 'product/')
        dispatch( {
            type: GET_PRODUCTOS,
            payload: res.data
        })
    }
    catch(e){
        dispatch( {
            type: PRODUCTOS_ERROR,
            payload: console.log(e),
        })
    }
}

export const addProductos = (data) => async dispatch => {
    try{
        const res = await axios.post(url +'product/', data)
        dispatch( {
            type: POST_PRODUCTOS,
            payload: res.data
        })
    }
    catch(e){
        dispatch( {
            type: PRODUCTOS_ERROR,
            payload: console.log(e),
        })
    }
}

export const updateProductos = (data) => async dispatch => {
    try{
        const res = await axios.put(url + `product/${data.id}`, data)
        dispatch( {
            type: UPDATE_PRODUCTOS,
            payload: res.data
        })
    }
    catch(e){
        dispatch( {
            type: PRODUCTOS_ERROR,
            payload: console.log(e),
        })
    }
}

export const deleteProductos = (data) => async dispatch => {
    try{
        const res = await axios.put(url +`/product/delect/${data.id}`, data)
        dispatch( {
            type: DELETE_PRODUCTOS,
            payload: res.data
        })
    }
    catch(e){
        dispatch( {
            type: PRODUCTOS_ERROR,
            payload: console.log(e),
        })
    }
}
