export const GET_PRODUCTOS = 'GET_PRODUCTOS';
export const POST_PRODUCTOS = 'POST_PRODUCTOS';
export const UPDATE_PRODUCTOS = 'UPDATE_PRODUCTOS';
export const DELETE_PRODUCTOS = 'DELETE_PRODUCTOS';
export const PRODUCTOS_ERROR = 'PRODUCTOS_ERROR'