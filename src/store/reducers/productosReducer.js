import {GET_PRODUCTOS, POST_PRODUCTOS, DELETE_PRODUCTOS, UPDATE_PRODUCTOS} from '../types'

const initialState = {
    productos:[],
    loading:true
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function(state = initialState, action){

    switch(action.type){

        case GET_PRODUCTOS:
            return {
                ...state,
                productos: action.payload,
                loading:false

            }
        case POST_PRODUCTOS:
            return {
                ...state,
                productos: action.payload,
                loading:false

            }
        case UPDATE_PRODUCTOS:
            return {
                ...state,
                productos: action.payload,
                loading:false

            }
        case DELETE_PRODUCTOS:
            return {
                ...state,
                productos: action.payload,
                loading:false

            }
        default: return state
    }

}